const DATA = [
	{
		id : 0,
		title : "Acai Berry Extract",
		price : 1,400,
		desc : "Protects the body from free radicals. Enhances memory and promotes skin elasticity. Loaded with antioxidants",
		image : "/assets/products/Acai.jpg",
		stocks: 100

	},
	{
		id : 1,
		title : "Pure Organic Barley",
		price : 900,
		desc : "Slows down the aging process, helps smoothen and whiten skin. Helps improve blood circulation and prevent arthritis. Destroys cancer cells and is a great source of antioxidants.",
		image : "/assets/products/Barley.jpg",
		stocks: 100
	},
	{
		id : 2,
		title : "Garcinia Cambogia",
		price : 800,
		desc : "Improves immune function. Boots metabolism. Lowers blood pressure and cholesterol. Aids the digestive system and appetite suppressant.",
		image : "/assets/products/Garcinia.jpg",
		stocks: 100
	},

	{
		id : 3,
		title : "Immunergy",
		price : 500,
		desc : "Immunity and energy boosting powerhouse, packed with the three (3) most sought-after nutrients for the immune system. Vitamin C, Vitamin D3 and Zinc, with the addition of Moringa Oleifera for the perfect boost of energy. ",
		image : "/assets/products/Immunergy.jpg",
		stocks: 100
	},


]

export default DATA;