import React from 'react';
import { NavLink } from 'react-router-dom';


const About = () => {
	return (
		<div>
			<section id="about">
				<div className="container my-5 py-5 mt-5 pt-5">
					<div className="row">
						<div className="col-md-6">
							<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
							  <div class="carousel-indicators">
							    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
							    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
							    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
							  </div>
							  <div className="carousel-inner" id="carousel">
							    <div className="carousel-item active">
							      <img src="/assets/aboutus.png" alt="About" />
							    </div>
							    <div className="carousel-item">
							      <img src="/assets/aboutus2.jpg" alt="About" />
							    </div>
							    <div className="carousel-item">
							      <img src="/assets/aboutus3.jpg" alt="About" />
							    </div>
							  </div>
							  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
							    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
							    <span class="visually-hidden">Previous</span>
							  </button>
							  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
							    <span class="carousel-control-next-icon" aria-hidden="true"></span>
							    <span class="visually-hidden">Next</span>
							  </button>
							</div>
						</div>
						<div className="col-md-6">
							<h3 className="fs-5 mb-0 bg-info" id="para">About Us</h3>
							<h1 className="display-6 mb-2 bg-info" id="para">Who <b>We</b> Are</h1>
							<hr className="w-50"/>
							<p className="lead mb-2" id="para">IAM Worldwide is a company committed to upgrading the quality of life – it was designed for dedicated, honest, hardworking, and self-motivated people who want to build their own businesses as Independent Distributors across the globe. Our portfolio offers a diverse and comprehensive range of marketing solutions, specialty products, and services aimed to drive individuals and families to financial excellence. </p>
							<p className="lead mb-2" id="para"><strong><b>Our Core Values</b></strong> are <b>Integrity First. Empowering People. Shaping Spirituality. Transforming Communities. Passion for Excellence</b>
							<hr/>
							<NavLink to="./contact" className="btn btn-outline-info rounded-pill px-5 offset-4 fw-bolder" id="para"><i className="fa fa-commenting-o me-2"></i> Contact Us</NavLink>
						</div>
					</div>
				</div>
			</section>
		</div>
		);
}


export default About;
