import React from 'react';
import Products from './Products';
import About from './About';
import Contact from './Contact';


const Home = () => {
	return (
		<div className="hero pt-5">
			<div className="card bg-dark text-white border-0">
			  <img src="/assets/backgroundimage.jpg" className="card-img" alt="Background" height="700px" />
			  <div className="card-img-overlay d-flex flex-column justify-content-center">
			  	<div className="container">
			    <h5 className="card-title display-3 fw-bolder py-3 my-0" id="text">THE MOST SUSTAINABLE & LIVEABLE COMPANY</h5>
			    </div>
			  </div>
			</div>
			<Products/>
			<About/>
			<Contact/>
		</div>
		);
}


export default Home;